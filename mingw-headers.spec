#%%global snapshot_date 20160723
#%%global snapshot_rev 65a0c3298db7cc5cbded63259663cb29e4780a56
#%%global snapshot_rev_short %(echo %snapshot_rev | cut -c1-6)
#%%global branch v5.x

#%%global pre rc2

# The mingw-w64-headers provide the headers pthread_time.h
# and pthread_unistd.h by default and are dummy headers.
# The real implementation for these headers is in a separate
# library called winpthreads. As long as winpthreads isn't
# available (and the old pthreads-w32 implementation is used)
# the flag below needs to be set to 1. When winpthreads is
# available then this flag needs to be set to 0 to avoid
# a file conflict with the winpthreads headers
# Winpthreads is available as of Fedora 20
%global bundle_dummy_pthread_headers 0

Name:           mingw-headers
Version:        9.0.0
Release:        4
Summary:        Win32/Win64 header files

License:        Public Domain and LGPLv2+ and ZPLv2.1
URL:            http://mingw-w64.sourceforge.net/
%if 0%{?snapshot_date}
# To regenerate a snapshot:
# Use your regular webbrowser to open https://sourceforge.net/p/mingw-w64/mingw-w64/ci/%{snapshot_rev}/tarball
# This triggers the SourceForge instructure to generate a snapshot
# After that you can pull in the archive with:
# spectool -g mingw-headers.spec
Source0:        http://sourceforge.net/code-snapshots/git/m/mi/mingw-w64/mingw-w64.git/mingw-w64-mingw-w64-%{snapshot_rev}.zip
%else
Source0:        http://downloads.sourceforge.net/mingw-w64/mingw-w64-v%{version}%{?pre:-%{pre}}.tar.bz2
%endif

# Our RPM macros automatically set the environment variable WIDL
# This confuses the mingw-headers configure scripts and causes various
# headers to be regenerated from their .idl source. Prevent this from
# happening as the .idl files shouldn't be used by default
Patch0:         mingw-headers-no-widl.patch

BuildArch:      noarch

BuildRequires:	make
BuildRequires:  mingw32-filesystem >= 95
BuildRequires:  mingw64-filesystem >= 95


%description
MinGW Windows cross-compiler Win32 and Win64 header files.


%package -n mingw32-headers
Summary:        MinGW Windows cross-compiler Win32 header files
Requires:       mingw32-filesystem >= 95
%if 0%{bundle_dummy_pthread_headers} == 0
Requires:       mingw32-winpthreads
%endif

Obsoletes:      mingw32-w32api < 3.17-3%{?dist}
Provides:       mingw32-w32api = 3.17-3%{?dist}

%description -n mingw32-headers
MinGW Windows cross-compiler Win32 header files.

%package -n mingw64-headers
Summary:        MinGW Windows cross-compiler Win64 header files
Requires:       mingw64-filesystem >= 95
%if 0%{bundle_dummy_pthread_headers} == 0
Requires:       mingw64-winpthreads
%endif

%description -n mingw64-headers
MinGW Windows cross-compiler Win64 header files.


%prep
%if 0%{?snapshot_date}
rm -rf mingw-w64-v%{version}
mkdir mingw-w64-v%{version}
cd mingw-w64-v%{version}
unzip %{S:0}
%autosetup -p1 -D -T -n mingw-w64-v%{version}/mingw-w64-mingw-w64-%{snapshot_rev}
%else
%autosetup -p1 -n mingw-w64-v%{version}%{?pre:-%{pre}}
%endif


%build
pushd mingw-w64-headers
    %mingw_configure --enable-sdk=all --enable-secure-api
popd


%install
pushd mingw-w64-headers
    %mingw_make_install DESTDIR=%{buildroot}
popd

# Drop the dummy pthread headers if necessary
%if 0%{?bundle_dummy_pthread_headers} == 0
rm -f %{buildroot}%{mingw32_includedir}/pthread_signal.h
rm -f %{buildroot}%{mingw32_includedir}/pthread_time.h
rm -f %{buildroot}%{mingw32_includedir}/pthread_unistd.h
rm -f %{buildroot}%{mingw64_includedir}/pthread_signal.h
rm -f %{buildroot}%{mingw64_includedir}/pthread_time.h
rm -f %{buildroot}%{mingw64_includedir}/pthread_unistd.h
%endif


%files -n mingw32-headers
%license COPYING DISCLAIMER DISCLAIMER.PD
%{mingw32_includedir}/*

%files -n mingw64-headers
%license COPYING DISCLAIMER DISCLAIMER.PD
%{mingw64_includedir}/*


%changelog
* Wed Feb 21 2024 yinhongchang<yinhongchang@kylinsec.com.cn> - 9.0.0-4
- Set bundle_dummy_pthread_headers from 1 to 0

* Mon Feb 5 2024 yinhongchang<yinhongchang@kylinsec.com.cn> - 9.0.0-3
- update version to 9.0.0

* Sat Jun 20 2020 Sandro Mani <manisandro@gmail.com> - 7.0.0-1
- Update to 7.0.0

* Thu Nov 12 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 6.0.0-5
- Set bundle_dummy_pthread_headers to 0

* Fri Oct 16 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 6.0.0-4
- Set bundle_dummy_pthread_headers to 1

* Fri Oct 09 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 6.0.0-3
- Initial version.
